package org.launchcode.tdd;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static junit.framework.TestCase.fail;


public class URLTests {
	String testString = "https://www.google.com/";
	URL testURL = new URL(testString);

	@Before
	public void init(){
		testString = "https://www.google.com/";
		testURL = new URL(testString);

	}

	@Test
	public void testConstructor(){
		assertEquals("www.google.com", testURL.getDomain());
		assertEquals("https", testURL.getProtocol());
		assertEquals("/", testURL.getPath());
	}

	@Test
	public void testToString(){
		assertEquals("https://www.google.com/", testURL.toString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testProtocolException(){
		testURL = new URL("httq://test.com/");
		fail("Should not get here, require valid URL");
	}
	@Test(expected = IllegalArgumentException.class)
	public void testDomainException(){
		testURL = new URL("http://te&st.com/");
		fail("Should not get here, require valid URL");
	}
	@Test(expected = IllegalArgumentException.class)
	public void testDomainException2(){
		testURL = new URL("http:///");
		fail("Should not get here, require valid URL");
	}
}
