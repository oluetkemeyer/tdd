package org.launchcode.tdd;

import java.util.Set;

public class URL {
    private final String domain;
    private final String path;
    private final String protocol;

    public URL(String url) throws IllegalArgumentException {
        char specialCharacters[] = {'!', '?', '>', '<', '/', '\\', '@', '#', '$', '%', '^', '&', '*', '(', ')', '[', ']', ':', ':', '\'', '\"', '+', '=', '`', '~'};
        String firstSplit[] = url.split("://");
        protocol = firstSplit[0].toLowerCase();
        if (firstSplit[1].split("/").length > 0){
            domain = firstSplit[1].split("/")[0].toLowerCase();
        }
        else{
            throw new IllegalArgumentException("Domain cannot be empty");
        }
        if (firstSplit[1].split("/").length > 1){
            path = "/" + firstSplit[1].split("/")[1].toLowerCase();
        }
        else{
            path = "/";
        }
        boolean test = (this.protocol.equals("https") || this.protocol.equals("http") || this.protocol.equals("ftp") || this.protocol.equals("file"));
        if (! test){
            throw new IllegalArgumentException("Not a valid protocol");
        }
        if (domain == null || domain.equals("") || domain.isEmpty()){
            throw new IllegalArgumentException("Domain cannot be empty");
        }
        for (char ch : domain.toCharArray()){
            for (char spec : specialCharacters){
                if (ch == spec){
                    throw new IllegalArgumentException("Domain cannot contain special characters");
                }
            }
        }
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public String getProtocol() {
        return protocol;
    }

    public String toString(){
        return this.protocol + "://" + this.domain + this.path;
    }
}
